const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Comment = require('./models/comment');
const Post = require('./models/post');
const User = require('./models/user');
const db = mongoose.connection;

const app = express();

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/api/posts', (req, res) => {
    Post.find().then((post) => {
        res.status(200).send({
            post
        });
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/api/comments', (req, res) => {
    Comment.find().then((comment) => {
        res.status(200).send({
            comment
        });
    }, (e) => {
        res.status(400).send(e);
    });
});

app.post('/api/add-user', (req, res) => {
    var user = new User({
        id: req.body.id,
        name: req.body.name,
        password: req.body.password
    })

    user.save().then((user) => {
        res.send(user);
      }, (e) => {
        res.status(400).send(e);
    });
});

app.post('/api/add-post', (req, res) => {
    var post = new Post({
        id: req.body.id,
        userId: req.body.userId,
        title: req.body.title,
        body: req.body.body
    })

    post.save().then((post) => {
        res.send(post);
      }, (e) => {
        res.status(400).send(e);
    });
});

app.post('/api/add-comment', (req, res) => {
    var comment = new Comment({
        id: req.body.id,
        userId: req.body.userId,
        name: req.body.name,
        body: req.body.body,
        postId: req.body.postId
    })

    comment.save().then((post) => {
        res.send(post);
      }, (e) => {
        res.status(400).send(e);
    });
});

app.post('/api/login', (req, res) => {
    var name = req.body.name;
    var password = req.body.password
    db.collection('users').find({ name, password }).toArray().then((user) => {
        res.send(user);
    }, (err) => {
        console.log("Unable to fetch", err);
    });
});

app.post('/api/user-exist', (req, res) => {
    var name = req.body.name;
    db.collection('users').find({ name }).count().then((count) => {
        res.status(200).send((count).toString());
    }, (err) => {
        console.log("Unable to fetch", err);
    });
});

app.get('/api/user-count', (req, res) => {
    db.collection('users').find().count().then((count) => {
        res.status(200).send((count).toString());
    }, (err) => {
        console.log("Unable to fetch", err);
    });
});

app.get('/api/posts-count', (req, res) => {
    db.collection('posts').find().count().then((count) => {
        res.status(200).send((count).toString());
    }, (err) => {
        console.log("Unable to fetch", err);
    });
});

app.get('/api/comments-count', (req, res) => {
    db.collection('comments').find().count().then((count) => {
        res.status(200).send((count).toString());
    }, (err) => {
        console.log("Unable to fetch", err);
    });
});

mongoose.connect('mongodb://localhost:27017/Blog', {
    useNewUrlParser: true
});



db.on('error', console.error.bind(console, 'Connection Error'))

app.listen(9000, () => {
    console.log('Running on port 9000')
});

module.exports = app;