const mongoose = require('mongoose');
var validator = require('validator');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    id: {
        type: Number,
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String
    },
    email: {
        type: String,
        minlength: 1,
        trim: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        trim: true,
        required: true
    }
})

module.exports = mongoose.model('User', userSchema);