const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
    postId: {
        type: Number,
        required: true
    },
    id: {
        type: Number
    },
    userId: {
        type: Number,
        required: true
    },
    body: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type: String
    }
})

module.exports = mongoose.model('Comment', commentSchema);