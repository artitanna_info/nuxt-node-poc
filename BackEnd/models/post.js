const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    userId: {
        type: Number,
        required: true
    },
    id: {
        type: Number
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    body: {
        type: String,
        trim: true,
        required: true
    },
    name: {
        type: String
    }
})

module.exports = mongoose.model('Post', postSchema);