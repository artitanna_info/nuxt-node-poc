import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    state: {
      PostList: [],
      filteredPostList: [],
      comments: [],
      isLoggedIn: false,
      userCount: 0,
      totalUserCount: 0,
      userInfo: {
        userName: '',
        userId: ''
      },
      loggedInValidation: false,
      postCount: 0,
      commentCount: 0
    },
    mutations: {
      setPostList (state, response) {
        state.PostList = response
      },
      setPostFilteredList (state, response) {
        state.filteredPostList = response
      },
      setComments (state, response) {
        state.comments = response
      },
      setLoggedIn (state, response) {
        state.isLoggedIn = response
      },
      setUserCount (state, response) {
        state.userCount = response
      },
      setTotalUserCount (state, response) {
        state.totalUserCount = response
      },
      setUserInfo (state, response) {
        state.userInfo.userName = response.name
        state.userInfo.userId = response.id
      },
      setLoggedInValidation (state, response) {
        state.loggedInValidation = response
      },
      setTotalPostCount (state, response) {
        state.postCount = response
      },
      setTotalCommentCount (state, response) {
        state.commentCount = response
      }
    },
    actions: {
      async getPostList ({ commit }) {
        await axios.get('http://localhost:9000/api/posts').then((res) => {
          commit('setPostList', res.data.post)
        }).catch(e => console.log(e.response))
      },
      async getComments ({ commit }) {
        await axios.get('http://localhost:9000/api/comments').then((res) => {
          console.log('axios--', res.data.comment)
          commit('setComments', res.data.comment)
        }).catch(e => console.log(e.response))
      },
      async addUser ({ commit }, req) {
        await axios.post('http://localhost:9000/api/add-user', {
          id: req.id,
          name: req.name,
          password: req.password
        }).then((res) => {
          commit('setLoggedIn', true)
          commit('setUserInfo', { name: res.data.name, id: res.data.id })
        }).catch(e => console.log(e.response))
      },
      async addPost ({ state, commit, dispatch }, req) {
        debugger
        await axios.post('http://localhost:9000/api/add-post', {
          id: req.id,
          userId: req.userId,
          title: req.title,
          body: req.body,
          name: req.userName
        }).then((res) => {
          const newPost = {
            title: res.data.title,
            body: res.data.body,
            name: res.data.name
          }
          dispatch('getPostList')
          console.log('state.PostList------', state.PostList)
          debugger
          state.PostList.push(newPost)
          console.log('-----', state.PostList.unshift(newPost))
          commit('setPostList', state.PostList.unshift(newPost))
        }).catch(e => console.log(e.response))
      },
      async addComment ({ state, commit, dispatch }, req) {
        debugger
        await axios.post('http://localhost:9000/api/add-comment', {
          id: req.id,
          userId: req.userId,
          postId: req.postId,
          body: req.body,
          name: req.name
        }).then((res) => {
          const newComment = {
            body: res.data.body,
            name: res.data.name,
            postId: res.data.postId
          }
          dispatch('getComments')
          console.log('state.setComments------', state.comments)
          debugger
          state.comments.push(newComment)
          commit('setComments', state.comments)
        }).catch(e => console.log(e.response))
      },
      async login ({ commit }, req) {
        await axios.post('http://localhost:9000/api/login', {
          name: req.name,
          password: req.password
        }).then((res) => {
          if (res.data.length === 0) {
            commit('setLoggedInValidation', true)
          } else {
            commit('setLoggedInValidation', false)
            commit('setLoggedIn', true)
            commit('setUserInfo', { name: res.data[0].name, id: res.data[0].id })
          }
        }).catch(e => console.log(e.response))
      },
      async userExist ({ commit }, req) {
        await axios.post('http://localhost:9000/api/user-exist', {
          name: req.name
        }).then((res) => {
          commit('setUserCount', res.data)
        }).catch(e => console.log(e.response))
      },
      async userCount ({ commit }) {
        await axios.get('http://localhost:9000/api/user-count').then((res) => {
          commit('setTotalUserCount', res.data)
        }).catch(e => console.log(e.response))
      },
      async postCount ({ commit }) {
        await axios.get('http://localhost:9000/api/posts-count').then((res) => {
          commit('setTotalPostCount', res.data)
        }).catch(e => console.log(e.response))
      },
      async commentCount ({ commit }) {
        await axios.get('http://localhost:9000/api/comments-count').then((res) => {
          commit('setTotalCommentCount', res.data)
        }).catch(e => console.log(e.response))
      }
    }
  })
}

export default createStore
